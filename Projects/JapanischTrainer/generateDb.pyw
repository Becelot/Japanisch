import tkinter
import codecs

def gen():
    out = codecs.open('database/db.csv', 'w', 'utf-8')
    out.write('Romanji;Hiragana')
    for line in li.curselection():
        print(line)
        temp = codecs.open('database/' + li.get(line).strip() + '.csv', 'r', 'utf-8')
        temp.readline()
        out.write(temp.read())
        temp.close()
    out.close()
    main.destroy()

def enter(e):
    gen()
    
main = tkinter.Tk()

series = open("database/series.nfo")
li = tkinter.Listbox(main, selectmode='multiple')
li.bind('<Return>', enter)

for line in series:
    li.insert("end", line)
    
li.pack()
button = tkinter.Button(main, text='Generate', command = enter)
button.pack()

li.focus()

main.mainloop()
